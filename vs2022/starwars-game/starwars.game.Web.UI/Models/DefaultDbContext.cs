﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace starwars.game.Web.UI.Models
{
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext(DbContextOptions options) : base(options)
        {
        }

        protected DefaultDbContext()
        {
        }

        /// <summary>
        /// C'est une LISTE ! et elle sera connectée à une table en bdd
        /// </summary>
        public DbSet<Game> Games { get; set; }
    }
}
