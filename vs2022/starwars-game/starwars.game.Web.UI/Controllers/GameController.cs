﻿using Microsoft.AspNetCore.Mvc;
using starwars.game.Web.UI.Models;
using System;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace starwars.game.Web.UI.Controllers
{
    public class GameController : Controller
    {
        private readonly DefaultDbContext context;
        public GameController(DefaultDbContext context)
        {
            this.context = context;
        }
        public IActionResult List()
        {
            var query = from item in this.context.Games
                        select item;

                var modelAEnvoyerALaView = query.ToList();

                return View(modelAEnvoyerALaView);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(GameAddViewModel item)
        {

            this.context.Games.Add(item.LaGame);
            //this.context.Personnes.Add(new Personne());
            this.context.SaveChanges();
            return RedirectToAction("List");
        }
    }

}
