﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tva.api
{
    public delegate decimal FournirTvaCourante();
    /// <summary>
    /// Affichage de info vers une sortie utilisateur
    /// </summary>
    /// <param name="info"></param>
    public delegate void AfficherInformation(object info);

    /// <summary>
    /// Récupère la saisie de l'utilisateur
    /// </summary>
    /// <returns></returns>
    public delegate string RecupererSaisie();
}
