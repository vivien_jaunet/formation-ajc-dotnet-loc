﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tva.api
{
    /// <summary>
    /// Classe qui permet de calculer la TVA
    /// </summary>
    /// <example></example>
    /// <remarks></remarks>
    public delegate decimal FournirTvaCourante();

    public class TvaCalculateur
    {

        public decimal CalculerTTC(decimal montantHT, decimal tva)
        {
            return montantHT * (1 + tva);
        }

        public decimal CalculerTTC(decimal montantHT, FournirTvaCourante getTva)
        {
            return montantHT * (1 + getTva);
        }

        public List<decimal> FiltrerTVA(List<decimal> tvas, decimal filtre = 0.055M)
        {
            return tvas.Where(item => item > filtre).ToList();
        }

        /// <summary>
        /// Demande la saisie de chaque TVA à insérer dans la liste
        /// </summary>
        /// <param name="tvas"></param>Liste de TVAs
        public void InitialiserListe(List<decimal> tvas, RecupererSaisie recupererSaisie, AfficherInformation afficher)
        {
            bool arretSaisie = false;

            do
            {
                afficher("Nouvelle TVA ? (STOP pour arrêter)");
                var saisieUtilisateur = recupererSaisie();

                arretSaisie = saisieUtilisateur == "STOP";
                if (!arretSaisie)
                {
                    if (decimal.TryParse(saisieUtilisateur, out var tva))
                    {
                        tvas.Add(tva);
                    }
                }
            } while (!arretSaisie);

        }
    }


}
