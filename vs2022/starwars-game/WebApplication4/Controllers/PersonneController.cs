﻿using Microsoft.AspNetCore.Mvc;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class PersonneController : Controller
    {
        private readonly DefaultDbContext context;
        // Parce que j'ai ajouté la ligne :
        // builder.Services.AddDbContext<DefaultDbContext>();
        public PersonneController(DefaultDbContext context)
        {
            this.context = context;
        }
        public IActionResult List()
        {
            //List<Personne> list = new()
            //{
            //   new() { Id= 1, Prenom = "Isidor" },
            //  new() { Id= 2, Prenom = "Batman" }
            //};
            var query = from item in this.context.Personnes
                        select item;

            var modelAEnvoyerALaView = query.ToList();

            return View(modelAEnvoyerALaView);
        }

        public IActionResult EditPersonne()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(PersonneAddViewModel item)
        {
            string value = this.Request.Form["chtulu"];

            this.context.Personnes.Add(item.LaPersonne);
            //this.context.Personnes.Add(new Personne());
            this.context.SaveChanges();
            return RedirectToAction("List");
        }
    }

    public class Pieuvre
    {
        public string Chtulu { get; set; }
    }

}
