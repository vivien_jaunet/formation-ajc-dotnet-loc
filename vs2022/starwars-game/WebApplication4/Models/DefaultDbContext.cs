﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace WebApplication4.Models
{
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext(DbContextOptions options) : base(options)
        {
        }

        protected DefaultDbContext()
        {
        }

        /// <summary>
        /// C'est une LISTE ! et elle sera connectée à une table en bdd
        /// </summary>
        public DbSet<Personne> Personnes { get; set; }

        //public static implicit operator ControllerContext(DefaultDbContext v)
        //{
            //throw new NotImplementedException();
        //}
    }
}
