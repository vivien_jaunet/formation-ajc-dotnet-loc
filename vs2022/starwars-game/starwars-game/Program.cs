﻿// See https://aka.ms/new-console-template for more information

using starwars_game.menu;
using starwars_game.menu.NouvellePartie;
using stgame.api;
using System.Runtime.ConstrainedExecution;
using System.Xml.Linq;

Console.ForegroundColor = ConsoleColor.DarkYellow;
var sousTitre = "A STAR WARS GAME".PadLeft(17);
Console.WriteLine("--------------------");
Console.WriteLine(sousTitre);
Console.WriteLine("--------------------");
Console.ForegroundColor = ConsoleColor.White;


void AfficherMenu()
{
    string[] menu = { // new string[] 
        "1. Nouvelle partie",
        "2. Charger partie",
        "0. Quitter"
    };

    foreach (var item in menu)
    {
        Console.WriteLine(item);
    }
}

int ChoisirMenu()
{
    int menuItem = 0;

    do
    {
        AfficherMenu();
        Console.ForegroundColor = ConsoleColor.DarkYellow;
        Console.WriteLine("--------------------");
        Console.ForegroundColor = ConsoleColor.White;
        Console.WriteLine("Ton choix ?");
        menuItem = int.Parse(Console.ReadLine());

        MenuItemType monChoix = (MenuItemType)menuItem;

        switch (monChoix)
        {
            case MenuItemType.DemarrerPartie:
                {
                    //DemarrerPartie = new DemarrerPartie();
                    DemarrerPartie();
                }
                break;

            case MenuItemType.ChargerPartie:
                {
                    ChargerPartie();
                }
                break;
        }
    } while (menuItem != 0);

    return menuItem;
}


int DemanderAge()
{
    bool ageValide = false;
    int vraiAge = 0;

    while (!ageValide)
    {
        Console.WriteLine("Ton age stp ?");
        var age = Console.ReadLine();

        ageValide = int.TryParse(age, out vraiAge);

        if (ageValide)
        {
            ageValide = vraiAge > 13;
        }
    }

    return vraiAge;
}

void DemarrerPartie()
{
    Console.WriteLine("Ton prénom stp ?");
    var prenom = Console.ReadLine();

    // DateTime maVraiDate;
    bool dateValide = false;
    do
    {
        Console.WriteLine("Ta date de naissance stp ?");
        var dateDeNaissance = Console.ReadLine();

        if (DateTime.TryParse(dateDeNaissance, out var maVraiDate))
        {
            var comparaisonDates = DateTime.Now - maVraiDate;
            int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
            Console.WriteLine($"Ton prénom est bien ? {prenom}, et tu es âgé de {nbAnnees} années ?");

            dateValide = true;

            int ageSaisi = DemanderAge();
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Erreur de saisie de date, ré-essaie stp !");
            Console.ForegroundColor = ConsoleColor.White;
        }
    } while (!dateValide);
}

void ChargerPartie()
{
    var CheckPoint = new CheckPoint("Check2403231530", true);
    var CheckPoint2 = new CheckPoint();

    Console.WriteLine(CheckPoint.Name);
    CheckPoint.PV = 12;

    Console.WriteLine(CheckPoint.Coordonnees);
    Console.WriteLine(CheckPoint.EstEnVie);

}



ChoisirMenu();
PreparerEnnemis();


void PreparerEnnemis()
{
    string[] nomAs =
    {
        "Dark Vador",
        "Boba fet",
        "Empereur Palpatine"
    };

    nomAs[0] = "Darth Vader";

    List<string> noms = new List<string>()
    {
        "Dark Vador",
        "Boba fet",
        "Empereur Palpatine"
    };
    noms.Add("Droide");
    noms.Remove("Droide");

    var ennemi = noms[0];
    int index = noms.IndexOf("Boba fet");
    ennemi = noms[index];

    List<int> powers = new List<int>()
    {
        1, 2, 5, 10, 20
    };

    var sum = powers.Sum(); // Linq
    var first = powers.Min();

    var min = nomAs.Min();

    DemandeAjoutEnnemie(noms);

    var query = from enem in noms
                let eneMaj = enem[0].ToString().ToUpper() + enem.Substring(1).ToLower()
                let premierCar = enem[0]
                let monObjet = new { /*Maj = eneMaj,*/ Initial = enem }
                //where enem.StartsWith("D") || enem.EndsWith("D")
                orderby enem descending
                select monObjet;

    /* var query2 = noms.Where(item => item.StartsWith("D") && item.EndsWith("D"))
                     .OrderByDescending(item => item)
                     .Select(item => item); */

    //query = query.Where(item => item.EndsWith("E"));


    

    foreach (var ennemy in query)
    {
       // Console.WriteLine(ennemy.Maj);
        Console.WriteLine(ennemy.Initial);
    }


    //var monObjetALaVolee = new
    //{
        //Prenom = "Anakin"
    //};
    //Console.WriteLine(monObjetALaVolee);
}

void DemandeAjoutEnnemie(List<string> lesEnnemis)
{
    const string ARRET_SAISIE = "STOP";
    string valeurSaisie = "";
    bool demandeArret = false;


    do
    {
        Console.WriteLine("Nouvel ennemi ? (STOP pour arrêter)");
        valeurSaisie = Console.ReadLine();
        demandeArret = valeurSaisie == ARRET_SAISIE;

        if (!demandeArret && !lesEnnemis.Contains(valeurSaisie))
        {
            lesEnnemis.Add(valeurSaisie);
        }
    } while (! demandeArret);
}

/* bool AjoutValide = false;
do
{
    Console.WriteLine("Quels sont les ennemies que vous voulez ajouter ? ");
    Console.ForegroundColor = ConsoleColor.DarkBlue;
    var AjoutEnnemies = Console.ReadLine();
    Console.ForegroundColor = ConsoleColor.White;

    if (AjoutEnnemies == "STOP")
    {
        AjoutValide = true;
    }
    else
    {

    }
}
while (!AjoutValide); */

Console.WriteLine(" ");
Console.WriteLine(" ");
Console.WriteLine("--------------------------------------------------------------------------------");
Console.WriteLine(" ");
Console.WriteLine(" ");




/* var nPartie = "Nouvelle partie".ToUpper();
var cPartie = "Charger partie".ToUpper();
var qGame = "Quitter".ToUpper();

Console.ForegroundColor = ConsoleColor.DarkGreen;
Console.WriteLine("MENU");
Console.ForegroundColor = ConsoleColor.White;
Console.WriteLine(nPartie);
Console.WriteLine(cPartie);
Console.WriteLine(qGame);

bool choiceValide = false;
do
{
    Console.WriteLine("Que souhaitez-vous faire ?");
    Console.ForegroundColor = ConsoleColor.DarkBlue;
    var choice = Console.ReadLine();
    Console.ForegroundColor = ConsoleColor.White;

    if (choice == qGame)
    {
        Console.WriteLine("Votre aventure s'arrête ici.");
        choiceValide = true;
    }
    else if (choice == nPartie)
    {
        Console.WriteLine("Comment vous appellez-vous ?");
        var name = Console.ReadLine();
        Console.WriteLine("Quelle est votre date de naissance ?");

        bool dateValide = false;
        do
        {
            Console.WriteLine("Ta date de naissance plz ?");
            var years = Console.ReadLine();

            if (DateTime.TryParse(years, out var maVraiDate))
            {
                var comparaisonDates = DateTime.Now - maVraiDate;
                int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
                dateValide = true;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Erreur de saisie de date");
                Console.ForegroundColor = ConsoleColor.White;

            }
        }
        while (!dateValide);


        Console.WriteLine($"Bonjour ");
        Console.ForegroundColor = ConsoleColor.DarkGreen;
        Console.Write(name);
        Console.ForegroundColor = ConsoleColor.White;
        Console.Write(" et bienvenue dans une nouvelle aventure");
        Console.WriteLine("Quel est votre âge ?");

        bool ageValide = false;
        do
        {
            var age = int.Parse(Console.ReadLine());

            if (age == nbAnnees && age > 13)
            {
                
                dateValide = true;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Erreur d'age");
                Console.ForegroundColor = ConsoleColor.White;

            }
        }
        while (!ageValide);

    }
    else
    {
        Console.ForegroundColor = ConsoleColor.DarkRed;
        Console.WriteLine("Vous devez faire l'un des choix ci-dessus.");
        Console.ForegroundColor = ConsoleColor.White;

    }
}
while (!choiceValide); */












//Random rand = new Random();
//Console.ForegroundColor = ConsoleColor.DarkGreen;
//string monTitre = "a starwars game".ToUpper();
//var monTitre = "a starwars game\n\n".ToUpper().PadLeft(70);
//Console.ForegroundColor = ConsoleColor.Yellow;
//var ssTitre = "A copycat of Zelda".ToLower().PadLeft(70);
//monTitre = monTitre + " (" + DateTime.Now.ToString() + ")";
//string format = "{0} ({1})";
//monTitre = string.Format(format, monTitre, DateTime.Now);
//Console.WriteLine(monTitre);
//Console.WriteLine(ssTitre);
//Console.ForegroundColor = ConsoleColor.White;
/*Console.WriteLine("Comment vous appellez-vous ?");
var name = Console.ReadLine();
Console.WriteLine("Quelle est votre date de naissance ?");


bool dateValide = false;
do
{
    Console.WriteLine("Ta date de naissance plz ?");
    var years = Console.ReadLine();

    if (DateTime.TryParse(years, out var maVraiDate))
    {
        var comparaisonDates = DateTime.Now - maVraiDate;
        int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
        Console.WriteLine($"Ton prénom est bien ? {name}, et tu es âgé de {nbAnnees} années ?");
        dateValide = true;
    }
    else
    {
        Console.ForegroundColor = ConsoleColor.DarkRed;
        Console.WriteLine("Erreur de saisie de date");
        Console.ForegroundColor = ConsoleColor.White;

    }
}
while (!dateValide);*/
//var yearsParse = DateTime.Parse(years);
//var years2 = (DateTime.Now - yearsParse).Days / 365;

//Console.WriteLine($"Ton prénom est bien {name} et tu es âgé de {years2} années.");

//Console.WriteLine($"{monTitre.ToUpper()} ({DateTime.Now.ToString("dd-MM-yyyy")})");


//foreach (char c in "A STARS WARS game !\n".ToUpper())
//{
//    Console.ForegroundColor = (ConsoleColor)rand.Next(1, 16);
//    Console.Write(c);
//}
//Console.ForegroundColor = ConsoleColor.White;

//Console.WriteLine("Hello, World!");

//void Calculer(int val1, int val2, out int result)
//{
//    {
//        int test = 0;
//    }

//    result = val1 + val2;
//}

//int resultat = 22;
//int un = 1;
//int deux = 2;
//Calculer(un, deux, out resultat);
//Console.WriteLine(resultat);