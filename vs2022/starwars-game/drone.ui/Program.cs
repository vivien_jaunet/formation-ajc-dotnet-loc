﻿using drone.api;

var drone = new Drone();

Console.WriteLine(drone.Loc);
Console.WriteLine(drone.Name);
Console.WriteLine(drone.Activite);
Console.WriteLine(drone.Etat);

drone.Loc = (12, 44);
drone.Name = "CV789";
drone.Activite = "En pause";
drone.Etat = "Aucun probleme";

var drone2 = new Drone();

drone2.Loc = (12, 44);
drone2.Name = "CV790";
drone2.Activite = "En pause";
drone2.Etat = "probleme";

var drone3 = new Drone();

drone3.Loc = (12, 44);
drone3.Name = "CV791";
drone3.Activite = "En activite";
drone3.Etat = "Aucun probleme";

var drone4 = new Drone();

drone4.Loc = (12, 44);
drone4.Name = "CV792";
drone4.Activite = "En pause";
drone4.Etat = "probleme";

Console.WriteLine(drone.Name);
Console.WriteLine(drone2.Name);
Console.WriteLine(drone3.Name);
Console.WriteLine(drone4.Name);

List<string> drones = new List<string>()
{
    drone.Name,
    drone.Activite,
    drone.Etat,
    "-------------",
    drone2.Name,
    drone2.Activite,
    drone2.Etat,
    "-------------",
    drone3.Name,
    drone3.Activite,
    drone3.Etat,
    "-------------",
    drone4.Name,
    drone4.Activite,
    drone4.Etat
};

AfficherDronesAC();


void AfficherDronesAC()
{

    var query = from drn in drones
                let ddrn = drone3.Activite
                select drn;
    Console.WriteLine("--------------------");
    Console.WriteLine("");
    foreach (var drones in query)
    {
        Console.Write("- ");
        Console.Write(drones);
        Console.WriteLine("");
    }
    Console.WriteLine("");
    Console.WriteLine("--------------------");



}
