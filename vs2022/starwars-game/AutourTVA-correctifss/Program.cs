﻿using tva.api;
using tva.api.displayers;

Console.OutputEncoding = System.Text.Encoding.UTF8;

//TvaCalculateur calculateur = new TvaCalculateur();
TvaCalculateur calculateur = new();
TvaDisplayer displayer = new();

List<decimal> tvaList = new();

void AfficherEnVert(object message)
{
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine(message);
    Console.ForegroundColor = ConsoleColor.White;
    displayer.Display(tvaList, Console.WriteLine);
}

var montantTTC = calculateur.CalculerTTC(20, () => 0.2M);

var tvasFiltrees = calculateur.FiltrerTVA(tvaList);
var tvasFiltrees2 = calculateur.FiltrerTVA(tvaList, 0.056M);
displayer.Display(tvasFiltrees, Console.WriteLine);

calculateur.InitialiserListe(tvaList, Console.ReadLine, AfficherEnVert);