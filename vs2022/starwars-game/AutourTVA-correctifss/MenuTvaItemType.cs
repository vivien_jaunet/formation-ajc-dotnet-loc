﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutourTva
{
    public enum MenuTvaItemType
    {
        AfficherTva = 1,
        AjouterTva = 2,
        Quitter = 0
    }
}
