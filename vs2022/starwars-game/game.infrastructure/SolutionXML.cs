﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using game.api.Adapters;

namespace game.infrastructure
{
    public class SolutionXML : IGameSauvegarde
    {
        public void CheminPourFichier()
        {
            var cheminFichier = Path.Combine(Environment.CurrentDirectory, "Sauvegarde.txt");
            var type = typeof(CheckPoint);
            // type.GetProperties()[0].Name;

            XmlSerializer serializer = new XmlSerializer(typeof(CheckPoint));

            using var streamF = new FileStream(cheminFichier, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            using var stream = new StreamWriter(cheminFichier);
            //using var mStream = new MemoryStream
            try
            {
                serializer.Serialize(stream, checkPoint);
            }
            finally
            {
                stream.Close();
            }


            using var streamR = new StreamReader(cheminFichier);
            checkPoint = serializer.Deserialize(streamF) as CheckPoint;

        }
    }
}
