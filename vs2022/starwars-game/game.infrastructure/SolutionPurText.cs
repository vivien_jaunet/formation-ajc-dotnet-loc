﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using game.api.Adapters;

namespace game.infrastructure
{
    public class SolutionPurText : IGameSauvegarde
    {
        public void CheminPourFichier(CheckPoint checkPoint)
        {

            var cheminFichier = Path.Combine(Environment.CurrentDirectory, "Sauvegarde.txt");
            try
            {
                File.WriteAllText(cheminFichier, checkPoint);
            }
            catch (PathTooLongException ex)
            {

            }
            catch (IOException ex)
            {

            }

        }
    }
}
