﻿using AutourTva;
using System;
using System.Linq;

List<string> tva = new List<string>()
    {
        "1.2",
        "1.055",
        "1.1"
    };

Console.ForegroundColor = ConsoleColor.DarkCyan;
var sousTitre = "MENU TVA".PadLeft(10);
Console.WriteLine("--------------------");
Console.WriteLine(sousTitre);
Console.WriteLine("--------------------");
Console.WriteLine("");
Console.ForegroundColor = ConsoleColor.White;

void MenuTva()
{
    string[] menu = { // new string[] 
        "1. Afficher",
        "2. Ajouter",
        "0. Quitter"
    };

    foreach (var item in menu)
    {
        Console.WriteLine(item);
    }
}



int ChoisirMenuTva()
{
    int menuItem = 0;
    

    do
    {
        MenuTva();
        Console.ForegroundColor = ConsoleColor.DarkCyan;
        Console.WriteLine("");
        Console.WriteLine("--------------------");
        Console.ForegroundColor = ConsoleColor.White;
        Console.WriteLine("Ton choix ?");
        Console.ForegroundColor = ConsoleColor.DarkGreen;
        menuItem = int.Parse(Console.ReadLine());
        Console.ForegroundColor = ConsoleColor.White;

        MenuTvaItemType monChoix = (MenuTvaItemType)menuItem;

        switch (monChoix)
        {
            case MenuTvaItemType.AfficherTva:
                {
                    AfficherTva();
                }
                break;

            case MenuTvaItemType.AjouterTva:
                {
                    AjoutTva(tva);
                }
                break;
        }
    } while (menuItem != 0);

    return menuItem;
}


ChoisirMenuTva();

void AfficherEnCouleur(object item, ConsoleColor couleur)
{
    Console.ForegroundColor = couleur;
    Console.WriteLine(item);
    Console.ForegroundColor = ConsoleColor.White;
}
void AfficherEnVert(object item)
{
    AfficherEnCouleur(item, ConsoleColor.Green);
}


void AfficherTva()
{

    var query = from pourc in tva
                select pourc;
    Console.WriteLine("--------------------");
    Console.WriteLine("");
    foreach (var tvas in query)
    {
        Console.Write("- ");
        Console.Write(tvas);
        Console.Write(" €");
        Console.WriteLine("");
    }
    Console.WriteLine("");
    Console.WriteLine("--------------------");

    

}

void AjoutTva(List<string> lesTvas)
{
    const string ARRET_SAISIE = "STOP";
    string valeurSaisie = "";
    bool demandeArret = false;


    do
    {
        Console.ForegroundColor = ConsoleColor.DarkGreen;
        Console.WriteLine("Quelles sont les différentes TVA existantes ? (STOP pour arrêter)");
        Console.ForegroundColor = ConsoleColor.White;
        valeurSaisie = Console.ReadLine();
        demandeArret = valeurSaisie == ARRET_SAISIE;

        if (!demandeArret && !lesTvas.Contains(valeurSaisie))
        {
            lesTvas.Add(valeurSaisie);
        }
    } while (!demandeArret);
}


void MontTTC()
{

}