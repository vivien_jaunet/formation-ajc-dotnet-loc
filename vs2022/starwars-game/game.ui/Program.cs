﻿using game.api;
using menu.api;
using starwars_game;


Menu menu = new(mess =>
{
    Console.ForegroundColor = ConsoleColor.DarkGreen;
    Console.WriteLine(mess);
    Console.ForegroundColor = ConsoleColor.White;
},
Console.ReadLine);

var game = new Game();
BasePersonnage persoPrincipal = new PersoPrincipal(Console.WriteLine, Console.ReadLine);

game.Demarrage += (Game arg1, DateTime arg2) =>
{
    Console.Write("Démarrage de la partie !");
};

game.Demarrer(persoPrincipal);

// Je vais attaquer

try
{
    game.Sauvegarder();
    // Je ferme
}
catch (NotImplementedException ex)
{
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine("Oops erreur !", ex.Message);
    Console.ForegroundColor = ConsoleColor.White;
}
catch (Exception ex)
{

}
finally
{
    Console.WriteLine("Execute obligatoire");
}




Console.ForegroundColor = ConsoleColor.DarkGreen;

var monTitre = "a starwars game".ToUpper();

Console.WriteLine($"{monTitre.ToUpper()} ({DateTime.Now.ToString("yyyy.MM.dd dddd")}) !");
Console.ForegroundColor = ConsoleColor.DarkCyan;
var sousTitre = "A zelda copy cat";
Console.WriteLine(sousTitre);
Console.ForegroundColor = ConsoleColor.White;


void AfficherMenu()
{
    var listMenu = Enum.GetValues(typeof(MenuItemType));
    foreach (var item in listMenu)
    {
        string itemTransforme = item.ToString().Replace("_", " ");
        Console.WriteLine("{0}: {1}", (int)item, itemTransforme);
    }

}

int ChoisirMenu()
{
    int menuItem = 0;

    do
    {
        AfficherMenu();

        Console.WriteLine("Ton choix ?");
        menuItem = int.Parse(Console.ReadLine());

        MenuItemType monChoix = (MenuItemType)menuItem;
        switch (monChoix)
        {
            case MenuItemType.Demarrer_Partie:
                {
                    DemarrerPartie();
                }
                break;

            case MenuItemType.Charger_Partie:
                {
                    ChargerPartie();
                }
                break;

        }
    } while (menuItem != 0);

    return menuItem;
}

void DemarrerPartie()
{
    Console.WriteLine("Ton prénom stp ?");
    var prenom = Console.ReadLine();

    bool dateValide = false;
    do
    {
        Console.WriteLine("Ta date de naissance stp ?");
        var dateDeNaissance = Console.ReadLine();

        if (DateTime.TryParse(dateDeNaissance, out var maVraiDate))
        {
            var comparaisonDates = DateTime.Now - maVraiDate;
            int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
            Console.WriteLine($"Ton prénom est bien ? {prenom}, et tu es âgé de {nbAnnees} années ?");

            dateValide = true;
            int ageSaisi = DemanderAge();
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Erreur de saisie de date, ré-essaie stp !");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
    while (!dateValide);
}

int DemanderAge()
{
    bool ageValide = false;
    int vraiAge = 0;

    while (!ageValide)
    {
        Console.WriteLine("Ton age stp ?");
        var age = Console.ReadLine();

        ageValide = int.TryParse(age, out vraiAge);

        if (ageValide)
        {
            ageValide = vraiAge > 13;
        }
    }

    return vraiAge;
}

void ChargerPartie()
{

}


PreparerEnnemis();
ChoisirMenu();


void PreparerEnnemis()
{
    string[,] multiDimensionnel = new string[2, 3];
    var item = multiDimensionnel[0, 0];

    List<string> noms = new List<string>()
    {
        "Dark Vador",
        "Boba fet",
        "Empereur Palpatine",
    };

    DemandeAjoutEnnemisParUtilisateur(noms);

    var query = from enem in noms
                let eneMaj = enem[0].ToString().ToUpper() + enem.Substring(1).ToLower()
                let premierCar = enem[0]
                let monObjet = new { Maj = eneMaj, Initial = enem }
                where enem.StartsWith("D") || enem.EndsWith("D")
                orderby enem descending
                select monObjet;

    foreach (var enemy in query)
    {
        Console.WriteLine(enemy.Maj);
        Console.WriteLine(enemy.Initial);
    }


}

void DemandeAjoutEnnemisParUtilisateur(List<string> lesEnnemis)
{
    const string ARRET_SAISIE = "STOP";
    string valeurSaisie = "";
    bool demandeArret = false;

    do
    {
        Console.WriteLine("Nouvel ennemi ? (STOP pour arrêter)");
        valeurSaisie = Console.ReadLine();

        demandeArret = valeurSaisie == ARRET_SAISIE;

        if (!demandeArret && !lesEnnemis.Contains(valeurSaisie))
        {
            lesEnnemis.Add(valeurSaisie);
        }

    } while (!demandeArret);

}