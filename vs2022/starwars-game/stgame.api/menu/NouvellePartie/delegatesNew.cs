﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.menu.NouvellePartie
{
    public class DemarrerPartie
    {
        public DemarrerPartie()
        {
            Console.WriteLine("Ton prénom stp ?");
            var prenom = Console.ReadLine();

            // DateTime maVraiDate;
            bool dateValide = false;
            do
            {
                Console.WriteLine("Ta date de naissance stp ?");
                var dateDeNaissance = Console.ReadLine();

                if (DateTime.TryParse(dateDeNaissance, out var maVraiDate))
                {
                    var comparaisonDates = DateTime.Now - maVraiDate;
                    int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
                    Console.WriteLine($"Ton prénom est bien ? {prenom}, et tu es âgé de {nbAnnees} années ?");

                    dateValide = true;

                    //int ageSaisi = DemanderAge();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Erreur de saisie de date, ré-essaie stp !");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            } while (!dateValide);
        }
    }
}
