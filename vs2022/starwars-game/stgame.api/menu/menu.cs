﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.menu
{
    public class menu
    {
        void AfficherMenu()
        {
            string[] menu = { // new string[] 
        "1. Nouvelle partie",
        "2. Charger partie",
        "0. Quitter"
    };

            foreach (var item in menu)
            {
                Console.WriteLine(item);
            }
        }

        int ChoisirMenu()
        {
            int menuItem = 0;

            do
            {
                AfficherMenu();

                Console.WriteLine("Ton choix ?");
                menuItem = int.Parse(Console.ReadLine());

                MenuItemType monChoix = (MenuItemType)menuItem;

                switch (monChoix)
                {
                    case MenuItemType.DemarrerPartie:
                        {
                            //DemarrerPartie = new DemarrerPartie();
                            //DemarrerPartie();
                        }
                        break;

                    case MenuItemType.ChargerPartie:
                        {
                            //ChargerPartie();
                        }
                        break;
                }
            } while (menuItem != 0);

            return menuItem;
        }
    }
}
