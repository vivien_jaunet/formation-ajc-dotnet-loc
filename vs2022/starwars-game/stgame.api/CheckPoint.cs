﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stgame.api
{
    public class CheckPoint
    {
        #region Fields
        private string checkDate = "10/03/23 18:03";
        private string name = "";
        private int pv;
        private object coordonnees = (15, 36);
        #endregion

        #region Constructors
        public CheckPoint() : this("", false) { }

        public CheckPoint(string name, object coordonnees) : this(name, 10)
        {
            this.coordonnees = coordonnees;
        }

        public CheckPoint(string name, int pv)
        {
            this.Name = name;

            this.PV = pv;
        }
        #endregion



        public string Name { get => name; set => name = value; }
        public int PV { get => pv; set => pv = value; }

        public bool EstEnVie => this.PV > 0;
        public bool EstEnVie2 { get { return this.PV > 0; } }


        public string CheckDate { get => checkDate; set => checkDate = value; }
        public object Coordonnees { get => coordonnees; set => coordonnees = value; }
    }
}
