﻿using zoo.api;

// avant héritage
//var animal = new Gorille(AnimalType.Gorille, "Chita", true);
//var animal2 = new Animal(AnimalType.Elephant, "Dumbo", true);

// ave héritage
var animal = new Gorille("Chita", true);
var animal2 = new Perroquet();

Console.WriteLine(animal.Poids);
animal.Poids = 12;

Console.WriteLine(animal.PositionX);
Console.WriteLine(animal.EstEnVie);



// animal.Type = AnimalType.Gorille; car le set en private dans la classe Animal
//animal.Type = AnimalType.Elephant;
